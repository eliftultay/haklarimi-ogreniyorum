//
//  QuizViewController.swift
//  haklarimiögreniyorum
//
//  Created by Elif Tultay on 8.04.2018.
//  Copyright © 2018 eliftultay. All rights reserved.
//

import UIKit
import AVFoundation

struct Question {
    var Question : String!
    var Answers : [String]!
    var Answer : Int!
}

class QuizViewController: UIViewController {
    
    var player:AVAudioPlayer = AVAudioPlayer()
    
    @IBOutlet weak var QLabel: UILabel!
    @IBOutlet var Buttons: [UIButton]!
    
    var selectedTest1 = Int()
    
    var selectedTest2 = Int()
    
    var QSet1 = [Question]()
    
    var QSet2 = [Question]()
    
    var QN1  = 1
    
    var QN2  = 1
    
    var AnswerNumber = Int()
    
    var select1 = Int()
    
    var select2 = Int()
    
    //var delete = Int()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        QSet1 = [Question(Question: "Ailesiyle göçebe bir yaşam sürdürmekte olan çocukların eğitim alması için devlet tarafından gerekli eğitim ulaşımı sağlanmaktadır. Bu durum aşağıdaki çocuk haklarından hangisinin devlet tarafından önemsendiğini gösterir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Kalıtım hakkı", "d) Korunma hakkı "], Answer:1),
                 Question(Question: "4.sınıf öğretmeni sınıf ortamında öğrencilerin düşüncelerini özgürce ifade edebileceği “Çocuk Mahkemesi” adlı etkinliğini oluşturmuştur. Bu durumda öğrenciler hangi haktan faydalanmıştır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "23 Nisan’da okul müdürünün koltuğuna oturan 9 yaşındaki Ahmet, okul içerisinde arkadaşlarıyla birlikte oynayacağı bir oyun alanı istemektedir. Ahmet’in bu isteği hangi çocuk hakkına sahip olduğunu ifade eder?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Küçük bir beldede yaşayan İrem, kitap okumayı çok sevmektedir. İrem’in yaşadığı beldede kitap bulabileceği bir kütüphane açılmıştır. Bu durumda devlet çocuklara hangi hakkı sağlamış olur?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Samet, aile içinde alınan kararlarda düşüncelerini ifade etmek istediğinde dikkate alınmamaktadır. Samet’in hangi hakkı göz ardı edilmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "18 yaş altındaki bireyleri çalıştırmaya zorlayan aileler devlet tarafından tespit edilerek uyarılmaktadırlar. Devlet bu durumda hangi çocuk hakkını önemsemektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Küçük bir ilçede yaşayan çocuklar için sosyal aktivitelerin yapıldığı bir merkez kurulmuştur. Devlet tarafından bu çocuklara hangi hak sağlanmıştır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Bir rehabilitasyon merkezinde öğretmen olan Rabia Hanım engelli öğrencilerinin kendilerine olan özgüvenlerini artırmak için gerekli eğitim çalışmaları planlamaktadır. Rabia Hanım’ın bu çalışmaları aşağıdakilerden hangi çocuk hakkını önemsediğini göstermektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "Ülkesinde yaşanan savaş sebebiyle ailesiyle birlikte başka bir yere göç etmek durumunda kalan 12 yaşındaki Melih, barınacak yer bulmakta güçlük çekmektedir. Bu durumda Melih’ in hangi hakkı göz ardı edilmiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "13 yaşındaki Mert, her sabah babasıyla tarlaya çalışmaya gittiği için okula gidememektedir. Mert’in elinden hangi hakkı alınmıştır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "3. Sınıfa gitmekte olan Emir ve Beren 20 Kasım Dünya Çocuk Hakları Günü’nde çocuk haklarıyla ilgili yeni projeler geliştirmiş ve bu çalışmalarını sınıf ortamında arkadaşlarıyla paylaşmışlardır. Emir ve Beren’in yeni projeler üretmesi hangi çocuk hakkının içerisinde yer alır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "“18 yaşın altındaki herkes çocuktur.” Çocukların çalıştırılmaması gerektiği hangi çocuk hakkının içerisinde yer alır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: " “Çocuklara dinleri, dilleri, cinsiyetleri vb. şeylerden dolayı ayrım yapılamaz”  ifadesi hangi hakkı ifade etmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "“Kendilerini ilgilendiren her konuda çocukların alınan kararlara katılması gerekir.” İfadesinde hangi temel haktan bahsedilmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "“Bir vatandaşlığa ve isme sahip olma hakkı her çocuğun hakkıdır.” ifadesi hangi çocuk hakkını ifade etmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "9 yaşındaki Ecrin ailesiyle çocuk tiyatrosuna gitmek istemektedir. Ecrin’in ailesi bu isteğini yerine getirerek Ecrin’in hangi hakkını önemsemiş olur?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: " 9 yaşındaki Özlem, okul kantinde sağlıksız yiyeceklerin satıldığını fark ederek sınıf öğretmenine bu durum hakkında bilgi vermiştir. Özlem’in bu davranışı hangi hakkının farkında olduğunu ifade eder?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "Tolga okul içerisinde çocuk haklarıyla ilgili topluluk kurmak istemektedir. Okul müdürü Tolga’nın bu topluluğu kurması için gerekli yardımları yapmaya hazırdır. Okul müdürü öğrencilerin hangi hakkına saygı göstermektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Düşüncelerini özgürce ifade eden ve haklarını bilen 12 yaşındaki Şeyma, arkadaşlarıyla birlikte okulda bir tiyatro ekibi kurmak istediğini belirterek hangi çocuk hakkından faydalanmak istemektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Ayça ailesiyle ilgili alınan kararlarda fikrini belirtmek istese de anne ve babası “Sen küçüksün, anlamazsın!” diyerek Ayça’ya söz hakkı vermezler. Ayça’nın hangi hakkı elinden alınmıştır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Şanlıurfa’da yaşayan 12 yaşındaki Timur, okul zamanında ailesiyle tarlaya pamuk toplamaya gitmektedir. Timur’un bu durumda hangi hakkı elinden alınmaktadır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "10 yaşındaki Ayşe; evinin karşısındaki oyun parkına gitmek istediğinde Ayşe’nin ailesi, hava koşulları uygunsa Ayşe’nin bu isteğini yerine getirmektedirler. Bu durumda Ayşe’nin ailesi hangi hakkı önemsemiş olur?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "9 yaşındaki Melike’nin anne ve babası bulundukları coğrafyada çıkan savaş nedeniyle Melike’yi okuldan alarak göç etmek zorunda kalmıştır. Melike göç ettiği ülkede yeniden okuluna devam ederek hangi çocuk hakkından faydalanmıştır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "8 yaşındaki Yusuf ailesiyle birlikte yaşamını sürdürdüğü ülkedeki savaş sebebiyle Türkiye’ye göç etmiştir. Türkiye’de bu durumda olan çocuklar için özel çalışmalar yürütülüp bu çocuklara yeni bir yaşam alanı verilmektedir. Bu durumda Yusuf’a devlet tarafından hangi hak verilmiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Yılmaz ailesinin en büyük çocuğu olan Murat 11 yaşındadır. Murat bir lokantada yarı zamanlı çalışarak ailesine maddi destekte bulunmaktadır. Murat iş yerinde çok çalışıp yorulmakta ve okuldaki derslerine gerektiği kadar çalışamadığından başarı sağlayamamaktadır. Bu durumda Murat’ın elinden alınan hak hangisidir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1)]
        
        QSet2 = [Question(Question: "Büşra 4.sınıfa gitmektedir. Arkadaşları tarafından okul temsilcisi olarak seçilen Büşra, okul müdürü tarafından verilen özel bir odada arkadaşlarının isteklerini dinleyip okul idaresiyle gerekli görüşmeleri yapmaktadır. Büşra bu durumda hangi temel hakkını kullanmaktadır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Kalıtım hakkı", "d) Korunma hakkı "], Answer:0),
                 Question(Question: "Ali; konuşkan, hakkını savunan, kendisine güvenen bir çocuktur. Derste öğretmen sınıftaki öğrencilere: ‘’Engelli bireyler hakkında ne düşünüyorsunuz?’’ diye soru sorduğunda Ali söz hakkı alarak düşüncelerini belirtmiş, çözüm önerileri sunmuştur. Ali bu durumda hangi temel hakkını kullanmıştır? ", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "“Yaşanılabilir bir dünyada yaşamak her çocuğun hakkıdır.” sözü hangi çocuk hakkına girer?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "“Anne ve babalarının izni olmadan hiçbir çocuk başka bir ülkeye götürülemez. Çocukları bu şekilde başka yerlere götüren kişilere karşı mücadele edilmesi gerekir.” ifadesinde hangi çocuk hakkından bahsedilmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Sağlık hizmetlerinden yararlanmak ve sağlıklı bir yaşam sürmek hangi çocuk hakkına girer?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "Okul müdürü Ersun Bey, çocukların kendilerini özgürce ifade edebileceği ve okulda gerçekleşmesini istedikleri hayalleri için dilek ve öneri kutusu oluşturmuştur. Bu istek ve önerileri dikkate alan okul müdürü Ersun Bey çocukların hangi hakkını gözetmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "11 yaşındaki Merve aile ortamından uzakta yaşayan, devlet tarafından özel olarak korunan bir çocuktur. Merve’nin de fikirlerini alarak ona koruyucu aile bulmak devletin görevlerindendir. Bu durumda devlet hangi çocuk hakkını gözetmektedir? ", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: " İlkokul 4.sınıf öğretmeni Saray Hanım il genelinde düzenlenen “Çocuk Hakları Resim Yarışması” hakkında öğrencileri bilinçlendirmiş ve bu konuda yetenekli öğrencileri yarışmaya katmıştır. Bu durumda Saray Hanım öğrencilerin hangi hakkını önemsemiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: " 9 yaşındaki Taha annesin: “Bugün aramıza İran’dan gelen bir arkadaş katıldı. Öğretmenimiz arkadaşımızın zor bir hayatının olduğunu, artık bizim okulda eğitim almaya devam edeceğini ve ona yardım etmemiz gerektiğini söyledi.”  Devletin İran’dan gelen çocuğa sağladığı hak aşağıdakilerden hangisidir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Hasta ve bakıma muhtaç çocukların eğitim ve öğretime devam etmesi için devlet tarafından özel eğitim desteği verilmektedir. Bu açıklamaya göre çocuklar hangi haktan faydalanmaktadır? ", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Öğretmen sınıf başkanlığı seçiminin yapılacağını söyler, Ali de sınıf başkanlığına aday olmak istediğini açıklar. Bu durumda Ali hangi hakkını kullanmaktadır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Devletin engellilere sağlamış olduğu destek eğitimi hangi hak kapsamında ele alınmaktadır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Üst sınıflardaki çocuklar okul içerisinde Tarık’a kötü davranmaktadır. Tarık öğretmenine yaşadığı bu olumsuz durumdan bahsederek yardım istemiştir. Tarık bu durumda hangi hakkını kullanmak istemiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Bisiklet yolunda kurallara uygun bir şekilde bisiklet süren Ceyda, trafik işaretlerine uymayan bir şoförü fark ederek durumu trafik polisine bildirmiştir. Bu durumda Ceyda hangi hakkının tehlikede olduğunu düşünmüştür?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "17 yaşındaki Kevser, kendisini takip eden ve şüpheli davranışlar sergileyen bir şahsı yakın çevresinde bulunan polise bildirerek hangi hakkını kullanmak istemiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Mahallede çocuklar top oynarken sokaktan geçen Selim Bey tarafından oyunları durdurulmuş ve topu ellerinden alarak oyun oynamalarına engel olmuştur. Selim Bey çocukların hangi hakkını ihlâl etmiştir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Devletin madde bağımlısı olan çocuklara ücretsiz tedavi imkânı sunması bu çocukların hangi hakkının önemsendiğini göstermektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "Aylin’in annesi oyun oynamanın zaman kaybı olduğunu söyleyerek Aylin’den sürekli ders çalışmasını istemektedir. Bu durumda Aylin’in annesi hangi hakkı göz ardı etmektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: " İnternet hizmeti sağlayıcılarının çocukların olumsuz ve zararlı içeriklere ulaşmalarını engellemek için bazı içeriklere erişim sınırı koymaları çocukların hangi hakları kapsamında değerlendirilebilir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Ailesinden tiyatro seçmelerine katılmak için izin isteyen Ceren, hangi hakkını kullanmak istemektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:3),
                 Question(Question: "Çocukların sağlıklı beslenip büyümelerini sağlamak için okullarda ücretsiz süt dağıtımı gerçekleşmektedir. Bu durum çocukların hangi hakkı kapsamında düşünülmüştür?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Zeyneplerin evinde çıkan yangına müdahalede hızlı olan itfaiye ekipleri, Zeynep ve ailesini güvenli bir bölgeye yerleştirmiştir. Görevliler Zeynep ve ailesinin hangi hakkını önemsemektedir? ", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:0),
                 Question(Question: "Beden eğitimi derslerinde öğrencilerinin özgürce oynaması, istedikleri sporları yapabilmelmesi adına  uygun ortam oluşturan öğretmenimiz çocukların hangi hakkını önemsemektedir?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1),
                 Question(Question: "Sosyal medya hesaplarında çocukların özel bilgilerinin (kimlik numarası, telefon numarası, ev adresi vb.)  yayınlanmaması gerektiği konusu hangi hak kapsamında yer almaktadır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:2),
                 Question(Question: "Okullarına ziyarete gelen kaymakamdan spor malzemeleri talep eden çocuklar, hangi hakları kapsamında bu istekte bulunmuşladır?", Answers: ["a) Yaşam hakkı", "b) Gelişim hakkı", "c) Korunma hakkı", "d) Katılım hakkı"], Answer:1)]
        
        //select = Int(arc4random_uniform(2))
        //delete = QSet1.count
        self.select1 = selectedTest1
        self.select2 = selectedTest2
        designButtons()
        PickQuestion()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func PickQuestion(){
        
        if select1 == 0 {
            
            QLabel.text = "Soru  \(QN1) )\n\n " + QSet1[0].Question
            AnswerNumber = QSet1[0].Answer
            
            for i in 0..<Buttons.count{
                Buttons[i].setTitle(QSet1[0].Answers[i], for: UIControlState.normal)
            }
            
            QSet1.remove(at: 0)
            
            QN1 += 1
            //delete = delete-1
            
            if QSet1.count == 0{
                performSegue(withIdentifier: "go", sender: nil)
            }
        } else {
            
        }
        
        if select2 == 1 {
            
            QLabel.text = "Soru  \(QN2) )\n \n " + QSet2[0].Question
            AnswerNumber = QSet2[0].Answer
            
            for i in 0..<Buttons.count{
                Buttons[i].setTitle(QSet2[0].Answers[i], for: UIControlState.normal)
            }
            
            QSet2.remove(at: 0)
            
            QN2 += 1
            //delete = delete-1
            
            if QSet2.count == 0{
                performSegue(withIdentifier: "go", sender: nil)
            }
        } else {
            
        }
    }
    
    @IBAction func Button(_ sender: Any) {
        
        if AnswerNumber == 0 {
            applause()
            Buttons[0].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[0].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        } else {
            failure()
            Buttons[0].backgroundColor = UIColor.red
            Buttons[AnswerNumber].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[0].backgroundColor = UIColor.babyBlue
                self.Buttons[self.AnswerNumber].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        }
    }
    
    @IBAction func Button1(_ sender: Any) {
        
        if AnswerNumber == 1 {
            applause()
            Buttons[1].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[1].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        } else {
            failure()
            Buttons[1].backgroundColor = UIColor.red
            Buttons[AnswerNumber].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[1].backgroundColor = UIColor.babyBlue
                self.Buttons[self.AnswerNumber].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        }
    }
    
    @IBAction func Button2(_ sender: Any) {
        
        if AnswerNumber == 2 {
            applause()
            Buttons[2].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[2].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        } else {
            failure()
            Buttons[2].backgroundColor = UIColor.red
            Buttons[AnswerNumber].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[2].backgroundColor = UIColor.babyBlue
                self.Buttons[self.AnswerNumber].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        }
    }
    
    @IBAction func Button3(_ sender: Any) {
        
        if AnswerNumber == 3 {
            applause()
            Buttons[3].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[3].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        } else {
            failure()
            Buttons[3].backgroundColor = UIColor.red
            Buttons[AnswerNumber].backgroundColor = UIColor.green
            for i in 0..<Buttons.count{
                Buttons[i].isEnabled = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.Buttons[3].backgroundColor = UIColor.babyBlue
                self.Buttons[self.AnswerNumber].backgroundColor = UIColor.babyBlue
                self.PickQuestion()
                for j in 0..<self.Buttons.count{
                    self.Buttons[j].isEnabled = true
                }
            }
        }
    }

    func designButtons(){
        
        QLabel.layer.cornerRadius = 5
        QLabel.layer.borderWidth = 1
        QLabel.layer.borderColor = UIColor.lightGray.cgColor
        
        for i in 0..<Buttons.count{
            Buttons[i].layer.cornerRadius = 5
            Buttons[i].layer.masksToBounds = true
            Buttons[i].layer.borderWidth = 1
            Buttons[i].layer.borderColor = UIColor.cyan.cgColor
        
        }
        
    }
    
    func applause(){
        
        do {
            let audioPath = Bundle.main.path(forResource: "applause", ofType: "mp3")
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPath!) as URL)
        }
        catch {
            //PROCESS ERROR!!!!
        }
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            
        }
        player.play()
    }
    
    func failure(){
        do {
            let audioPath = Bundle.main.path(forResource: "failure", ofType: "mp3")
            try player = AVAudioPlayer(contentsOf: NSURL(fileURLWithPath: audioPath!) as URL)
        }
        catch {
            //PROCESS ERROR!!!!
        }
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback)
        }
        catch {
            
        }
        player.play()
    }
   
}







