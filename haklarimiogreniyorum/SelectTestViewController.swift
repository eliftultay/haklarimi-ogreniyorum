//
//  SelectTestViewController.swift
//  haklarimiogreniyorum
//
//  Created by Elif Tultay on 18.11.2018.
//  Copyright © 2018 eliftultay. All rights reserved.
//

import UIKit

class SelectTestViewController: UIViewController {
    
    @IBOutlet weak var test1Button: UIButton!
    @IBOutlet weak var test2Button: UIButton!
    
    var test1:Int = 0
    var test2:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designButtons()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "testOneSegue") {
            let QuizVC: QuizViewController = segue.destination as! QuizViewController
            QuizVC.selectedTest1 = self.test1
        }
        
        if (segue.identifier == "testTwoSegue"){
            let QuizVC: QuizViewController = segue.destination as! QuizViewController
            QuizVC.selectedTest2 = self.test2
        }
    }
    
    func designButtons(){
        
        test1Button.layer.cornerRadius = 5
        test1Button.backgroundColor = UIColor.babyBlue
        test1Button.layer.masksToBounds = true
        test1Button.layer.borderWidth = 1
        test1Button.layer.borderColor = UIColor.cyan.cgColor
        
        
        test2Button.layer.cornerRadius = 5
        test2Button.backgroundColor = UIColor.babyBlue
        test2Button.layer.masksToBounds = true
        test2Button.layer.borderWidth = 1
        test2Button.layer.borderColor = UIColor.cyan.cgColor
        
    }
    
}










